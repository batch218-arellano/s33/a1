
fetch('https://jsonplaceholder.typicode.com/todos') 


.then((response)=> response.json())
.then((json) => console.log(json));


///---- 

fetch('https://jsonplaceholder.typicode.com/todos/1')
.then((response)=>response.json())
.then((json) => console.log(`The item "${json.title}" on the list has a status of ${json.completed}`));
// POST 

fetch ('https://jsonplaceholder.typicode.com/todos',{
	method: 'POST',
	headers :{
		'Content-type' : 'application/json'
	},
	body: JSON.stringify({
		title : 'Created to Do list Item',
		completed: 'false',
		userId: '1',
		id: '201'
	})

})

.then((response)=> response.json())
.then((json) => console.log(json));
/// PUT 


fetch('https://jsonplaceholder.typicode.com/todos/1', 
{
	method: 'PUT',
	headers: {
		'Content-type' : 'application/json'
	},
	body: JSON.stringify({
		title: 'Updated To Do List Item',
		description: 'To update the my to do list with a different data structure.',
		status:'Pending',
		dateCompleted:'Pending' ,
		userId: 1,
	})	
})
.then((response)=>response.json())
.then((json)=> console.log(json));

// patch method 


fetch('https://jsonplaceholder.typicode.com/todos/1',{
	method: "PATCH",
	headers: {
		'Content-type' : 'application/json'
	}, 
	body: JSON.stringify({
		dateCompleted: "07/09/21",
		status: "Complete",
	})	
})
.then((response)=>response.json())
.then((json)=> console.log(json));

// delete


fetch("https://jsonplaceholder.typicode.com/todos/2",{
	method: "DELETE",
})
.then((response)=>response.json())
.then((json)=> console.log(json));


